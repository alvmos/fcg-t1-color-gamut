#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "glut.h"
#include "color.h"

#define LAMBDA_MIN 380
#define LAMBDA_MAX 780
#define LAMBDA_COUNT (LAMBDA_MAX - LAMBDA_MIN + 1)

int width = 300, height = 300;
float rotation[16] = {
	1, 0, 0, 0, 
	0, 1, 0, 0, 
	0, 0, 1, 0,
	0, 0, 0, 1};
float eye_distance = 4.0;

float pointsXYZ[(LAMBDA_COUNT - 1) * LAMBDA_COUNT][3];
float pointsRGB[(LAMBDA_COUNT - 1) * LAMBDA_COUNT][3];
float colors[(LAMBDA_COUNT - 1) * LAMBDA_COUNT][3];
int XYZ = 1, triangles = 0;

void
generatePoints()
{
	printf("Generating points and colors...");

	float reflectance[LAMBDA_COUNT];
	// Fill the spectrum with 0.0
	for (int i = 0; i < LAMBDA_COUNT; i++)
		reflectance[i] = 0.0;
	// Enumerate the spectrums
	int i, deltha_lambda, lambda0;
	for (i = 0, deltha_lambda = 1, lambda0 = LAMBDA_MIN; i < LAMBDA_COUNT * (LAMBDA_COUNT - 1); i++)
	{
		int lambda0_index = lambda0 - LAMBDA_MIN;
		reflectance[(lambda0_index + deltha_lambda - 1) % LAMBDA_COUNT] = 1.0;
		// The spectrum is ready, calculate the points
		corCIEXYZfromSurfaceReflectance(LAMBDA_MIN, LAMBDA_MAX - LAMBDA_MIN + 1, 1, reflectance, &pointsXYZ[i][0], &pointsXYZ[i][1], &pointsXYZ[i][2], D65);
		corCIEXYZtoCIERGB(pointsXYZ[i][0], pointsXYZ[i][1], pointsXYZ[i][2], &pointsRGB[i][0], &pointsRGB[i][1], &pointsRGB[i][2]);
		corCIEXYZtosRGB(pointsXYZ[i][0], pointsXYZ[i][1], pointsXYZ[i][2], &colors[i][0], &colors[i][1], &colors[i][2], D65);
		// Getting ready for the next iteration
		if (lambda0 == LAMBDA_MAX)
		{
			reflectance[(lambda0_index + deltha_lambda) % LAMBDA_COUNT] = 1.0;
			deltha_lambda++;
		}
		reflectance[lambda0_index] = 0.0;
		lambda0 = (lambda0 == LAMBDA_MAX) ? LAMBDA_MIN : (lambda0 + 1);
	}

	printf("done\n");
}

void
output(int x, int y, const char *string)
{
	int len, i;

	glRasterPos2f(x, y);
	len = (int)strlen(string);
	for (i = 0; i < len; i++) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, string[i]);
	}
}

void
drawLines()
{
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < (LAMBDA_COUNT - 1) * LAMBDA_COUNT; i++)
	{
		if (i % LAMBDA_COUNT == 0)
		{
			glEnd();
			glBegin(GL_LINE_STRIP);
		}
		glColor3fv(colors[i]);
		if (XYZ)
			glVertex3fv(pointsXYZ[i]);
		else
			glVertex3fv(pointsRGB[i]);
	}
	glEnd();
}

void
drawPoints()
{
	glBegin(GL_POINTS);
	for (int i = 0; i < (LAMBDA_COUNT - 1) * LAMBDA_COUNT; i++)
	{
		glColor3fv(colors[i]);
		if (XYZ)
			glVertex3fv(pointsXYZ[i]);
		else
			glVertex3fv(pointsRGB[i]);
	}
	glEnd();
}

void
drawTriangles()
{
	for (int n = 0; n < LAMBDA_COUNT - 2; n++)
	{
		glBegin(GL_TRIANGLE_STRIP);
		for (int i = 0, j = 0, pi = 1; i < LAMBDA_COUNT; j += 1 - pi, pi = 1 - pi, i += 1 - pi)
		{
			if (pi)
			{
				glColor3fv(colors[n * LAMBDA_COUNT + i]);
				if (XYZ)
					glVertex3fv(pointsXYZ[n * LAMBDA_COUNT + i]);
				else
					glVertex3fv(pointsRGB[n * LAMBDA_COUNT + i]);
			}
			else
			{
				glColor3fv(colors[(n + 1) * LAMBDA_COUNT + j]);
				if (XYZ)
					glVertex3fv(pointsXYZ[(n + 1) * LAMBDA_COUNT + j]);
				else
					glVertex3fv(pointsRGB[(n + 1) * LAMBDA_COUNT + j]);
			}
		}
		glEnd();
	}
}

void
drawText()
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, width - 1, height - 1, 0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	output(10, 20, "+/-"); output(60, 20, "- Zoom in/out");
	output(10, 35, "s"); output(60, 35, "- Switch between points and triangles");
	output(10, 50, "space"); output(60, 50, "- Switch between CIE-XYZ and CIE-RGB");
	output(10, 65, "[shift+]x"); output(60, 65, "- Rotate [-]1 degree (x-axis)");
	output(10, 80, "[shift+]y"); output(60, 80, "- Rotate [-]1 degree (y-axis)");
	output(10, 95, "[shift+]z"); output(60, 95, "- Rotate [-]1 degree (z-axis)");

	output(width / 2 - 30, height - 20, XYZ ? "[CIE-XYZ]" : "[CIE-RGB]");
	
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}

void
drawAxis(float length)
{
	glBegin(GL_LINES);
	
	// x-axis
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(length, 0.0, 0.0);

	// White lines parallels to x-axis
	glColor3f(1.0, 1.0, 1.0);

	glVertex3f(0.0, 0.0, length);
	glVertex3f(length, 0.0, length);

	glVertex3f(0.0, length, 0.0);
	glVertex3f(length, length, 0.0);

	glVertex3f(0.0, length, length);
	glVertex3f(length, length, length);

	// y-axis
	glColor3f(0.0, 1, 0.0);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(0.0, length, 0.0);

	// White lines parallels to y-axis
	glColor3f(1.0, 1.0, 1.0);

	glVertex3f(length, 0.0, length);
	glVertex3f(length, length, length);

	glVertex3f(length, 0.0, 0.0);
	glVertex3f(length, length, 0.0);

	glVertex3f(0.0, 0.0, length);
	glVertex3f(0.0, length, length);

	// z-axis
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(0.0, 0.0, length);

	// White lines parallels to z-axis
	glColor3f(1.0, 1.0, 1.0);

	glVertex3f(length, 0.0, 0.0);
	glVertex3f(length, 0.0, length);

	glVertex3f(length, length, 0.0);
	glVertex3f(length, length, length);

	glVertex3f(0.0, length, 0.0);
	glVertex3f(0.0, length, length);

	glEnd();
}

void
display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	drawAxis(1.0);
	if (triangles)
		drawTriangles();
	else
		drawPoints();

	drawText();
	
	glutSwapBuffers();
}

void
keyboard(unsigned char c, int x, int y)
{
	glLoadMatrixf(rotation);
	switch (c)
	{
	case 'X':
		glRotatef(-1, 1, 0, 0);
		break;
	case 'x':
		glRotatef(1, 1, 0, 0);
		break;
	case 'Y':
		glRotatef(-1, 0, 1, 0);
		break;
	case 'y':
		glRotatef(1, 0, 1, 0);
		break;
	case 'Z':
		glRotatef(-1, 0, 0, 1);
		break;
	case 'z':
		glRotatef(1, 0, 0, 1);
		break;
	case ' ':
		XYZ = 1 - XYZ;
		break;
	case 's':
		triangles = 1 - triangles;
		break;
	case '+':
		eye_distance -= 0.1;
		break;
	case '-':
		eye_distance += 0.1;
		break;
	default:
		break;
	}
	glGetFloatv(GL_MODELVIEW_MATRIX, rotation);
	
	glLoadIdentity();
	gluLookAt(0.5, 0.5, eye_distance, 0.5, 0.5, 0.5, 0.0, 1.0, 0.0);
	
	glTranslatef(0.5, 0.5, 0.5);
	glMultMatrixf(rotation);
	glTranslatef(-0.5, -0.5, -0.5);

	glutPostRedisplay();
}

void
resize(int w, int h)
{
	width = w;
	height = h;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(40, (double)(width * 1.0 / height), 1, 10);
	glMatrixMode(GL_MODELVIEW);
}

void
init(void)
{
	// Use depth buffering for hidden surface elimination
	glEnable(GL_DEPTH_TEST);

	// Setup the view
	glMatrixMode(GL_PROJECTION);
	gluPerspective( /* fov */ 40.0,	/* aspect ratio */ 1.0,	/* Z near */ 1.0, /* Z far */ 10.0);
	glMatrixMode(GL_MODELVIEW);
	gluLookAt(
		0.5, 0.5, eye_distance,		// eye is at (0.5,0.5,5.0)
		0.5, 0.5, 0.5,		// center is at (0.5,0.5,0.5)
		0.0, 1.0, 0.0);		// up is in positive Y direction
	glMultMatrixf(rotation);
		
	generatePoints();
}

int
main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 600);

	glutCreateWindow("T1 - Gammut de cores");
	
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(resize);
	
	init();

	glutMainLoop();
	return 0;             /* ANSI C requires main to return int. */
}